<?php 


function randomize_IDs($list) {
	foreach($list as $id => $value) {
		$randomized_IDs[] = $id;
	}
	
	shuffle($randomized_IDs);
	return $randomized_IDs;
}

function retrieve_Student($id, $class_roster_array) {

	return $class_roster_array[$id]["name"];
}

function next_Student($current_student_id, $student_order) {
	$current_id = array_search($current_student_id, $student_order);
	//echo $current_id;

	if($current_id == count($student_order) - 1 ){

		$next_student_id = 0;	
	} else {
		$next_student_id = $current_id + 1;
	}
	//echo $student_order[$next_student_id];
	//echo $next_student_id;
	return $student_order[$next_student_id];
	
}

// 1. make a function called randomize_IDs which:
//    a. store the ids from the passed in list (e.g. student-01) in an array
//    b. randomize the order of the newly created id array
//    c. return the randomized id array 
//      
//  note that this function should not alter the original list
//
//  e.g. function randomize_IDs($list){
//         ...your code here...
//         return $randomized_id_list; //=> ["student-03", "student-07", etc...]
//       }

// -------------------

// 2. make a function called retrieve_Student which:
//     a. accepts as its first argument a particular id to retrieve
//     b. accepts as its second argument a list of items in the format of class-roster.json
//     c. returns a student's name based on the id given
//
//  e.g. retrieve_Student("student-01", $class_roster_array) //=> "Lawrence Arden"

// -------------------

// 3. make a function called next_Student which:
//     a. accepts as its first argument the current student
//     b. accepts as its second argument the student order array
//     c. return the id of the next member of the student order array
//         if the current student is the last member of the list,
//         the list should start over
//
//  e.g. function next_Student($current_student_id, $student_order){
//         ...your code here...
//         return $student_order[$next_student_id];
//       }

